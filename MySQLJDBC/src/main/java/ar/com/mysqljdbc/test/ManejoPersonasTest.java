package ar.com.mysqljdbc.test;

import ar.com.mysqljdbc.datos.*;
import ar.com.mysqljdbc.domain.PersonaDTO;
import java.sql.*;
import java.util.List;

public class ManejoPersonasTest {

    public static void main(String[] args) {

        Connection conexiontransaccional = null;
        try {
            conexiontransaccional = Conexion.getConnection();
            if (conexiontransaccional.getAutoCommit()) {
                conexiontransaccional.setAutoCommit(false);
            }

            IPersonaDao personaDao = new PersonaDaoImpl(conexiontransaccional);
            List<PersonaDTO> personas = personaDao.select();

            for (PersonaDTO persona : personas) {
                System.out.println("Primera iteracion previo a cambios");
                System.out.println(persona);
            }

            PersonaDTO persona1 = new PersonaDTO();
            persona1.setId_persona(4);
            persona1.setNombre("Pedro");
            persona1.setApellido("Juarez");
            persona1.setEmail("pjuarez@mail.com");
            persona1.setTelefono("4560987");
            personaDao.update(persona1);

            PersonaDTO persona2 = new PersonaDTO();
            persona2.setNombre("Julio Ricardo");
            persona2.setApellido("Larriera");
            persona2.setEmail("jrlarrieras@mail.com");
            persona2.setTelefono("1010101010");
            personaDao.insert(persona2);

            PersonaDTO persona3 = new PersonaDTO();
            persona3.setId_persona(6);
            personaDao.delete(persona3);

            personas = personaDao.select();
            for (PersonaDTO persona : personas) {
                System.out.println("Segunda iteracion luego de los cambios");
                System.out.println(persona);
            }
            conexiontransaccional.commit();
            System.out.println("Se ha hecho commit de la transaccion");

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            System.out.println("Realizando rollback");
            try {
                conexiontransaccional.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace(System.out);
            }

        }

    }

}
