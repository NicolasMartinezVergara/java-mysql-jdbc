package ar.com.mysqljdbc.test;

import ar.com.mysqljdbc.datos.*;
import ar.com.mysqljdbc.domain.UsuarioDTO;
import java.sql.*;
;
import java.util.*;



public class ManejoUsuariosTest {

    public static void main(String[] args) {

        Connection conexiontransaccional = null;

        try {
            conexiontransaccional = Conexion.getConnection();
            if (conexiontransaccional.getAutoCommit()) {
                conexiontransaccional.setAutoCommit(false);
            }

            IUsuarioDao usuarioDao = new UsuarioDaoImpl(conexiontransaccional);
            List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();

            UsuarioDTO usuario1 = new UsuarioDTO("jrlarriera@mail.com", "Ks9876");
            usuarioDao.insert(usuario1);
            
            UsuarioDTO usuario2 = new UsuarioDTO("mlopez@mail.com", "4556He");
            usuarioDao.insert(usuario2);
            
            UsuarioDTO usuario3 = new UsuarioDTO(2, "pjuarez@mail.com", "123Mg");
            usuarioDao.update(usuario3);
            
            UsuarioDTO usuario4 = new UsuarioDTO(9);
            usuarioDao.delete(usuario4);

            usuarios = usuarioDao.select();
            for (UsuarioDTO usuario : usuarios) {
                System.out.println(usuario);
            }
            conexiontransaccional.commit();
            System.out.println("Realizando commit conexion transaccional");

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            try {
                conexiontransaccional.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace(System.out);
                System.out.println("Ejecutando rollback");
            }
        }

    }

}
