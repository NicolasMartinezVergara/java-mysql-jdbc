package ar.com.mysqljdbc.datos;

import ar.com.mysqljdbc.domain.PersonaDTO;
import java.sql.*;
import java.util.*;


public class PersonaDaoImpl implements IPersonaDao {

    private static final String SQL_SELECT = "SELECT id_persona, nombre, apellido, email, telefono FROM persona";
    private static final String SQL_INSERT = "INSERT INTO persona(nombre, apellido, email, telefono) VALUES(?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE persona SET nombre = ?, apellido = ?, email = ?, telefono = ? WHERE id_persona = ?";
    private static final String SQL_DELETE = "DELETE FROM persona WHERE id_persona = ?";
    
    private Connection conexiontransaccional;

    public PersonaDaoImpl() {

    }
    
    public PersonaDaoImpl(Connection conexiontransaccional){
        this.conexiontransaccional = conexiontransaccional;
    }

    @Override
    public List<PersonaDTO> select() {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        PersonaDTO persona = null;
        List<PersonaDTO> personas = new ArrayList<PersonaDTO>();
        try {
            conn = this.conexiontransaccional != null ? this.conexiontransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int id_persona = rs.getInt("id_persona");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String email = rs.getString("email");
                String telefono = rs.getString("telefono");

                persona = new PersonaDTO();
                persona.setId_persona(id_persona);
                persona.setNombre(nombre);
                persona.setApellido(apellido);
                persona.setEmail(email);
                persona.setTelefono(telefono);

                personas.add(persona);
            }

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            Conexion.close(rs);
            Conexion.close(stmt);
            if(this.conexiontransaccional == null){
                Conexion.close(conn);
            }
            

        }

        return personas;

    }

    @Override
    public int insert(PersonaDTO persona) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexiontransaccional != null ? this.conexiontransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setString(1, persona.getNombre());
            stmt.setString(2, persona.getApellido());
            stmt.setString(3, persona.getEmail());
            stmt.setString(4, persona.getTelefono());

            rows = stmt.executeUpdate();
            System.out.println("Numero de registros creados: " + rows);

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            Conexion.close(stmt);
            if(this.conexiontransaccional == null){
               Conexion.close(conn);

            }
        }

        return rows;
    }

    @Override
    public int update(PersonaDTO persona) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexiontransaccional != null ? this.conexiontransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_UPDATE);
            stmt.setString(1, persona.getNombre());
            stmt.setString(2, persona.getApellido());
            stmt.setString(3, persona.getEmail());
            stmt.setString(4, persona.getTelefono());
            stmt.setInt(5, persona.getId_persona());

            rows = stmt.executeUpdate();
            System.out.println("Numero de registros actualizados: " + rows);

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(stmt);
            if(this.conexiontransaccional == null){
                Conexion.close(conn);
            }
        }

        return rows;

    }

    @Override
    public int delete(PersonaDTO persona) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
        
        try {
            conn = this.conexiontransaccional != null ? this.conexiontransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_DELETE);
            stmt.setInt(1, persona.getId_persona());
            rows = stmt.executeUpdate();
            System.out.println("Número de registros eliminados: " + rows);
            
                       
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally{
            Conexion.close(stmt);
            if(this.conexiontransaccional == null){
                Conexion.close(conn);
            }
            
        }
        
        return rows;
    }

}
