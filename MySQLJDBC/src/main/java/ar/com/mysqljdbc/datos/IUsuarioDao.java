
package ar.com.mysqljdbc.datos;

import ar.com.mysqljdbc.domain.UsuarioDTO;
import java.util.List;

public interface IUsuarioDao {
    
    public List<UsuarioDTO> select();
    
    public int insert(UsuarioDTO usuario);
    
    public int update(UsuarioDTO usuario);
    
    public int delete(UsuarioDTO usuario);
    
}
