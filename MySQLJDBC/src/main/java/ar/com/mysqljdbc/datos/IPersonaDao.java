
package ar.com.mysqljdbc.datos;

import ar.com.mysqljdbc.domain.PersonaDTO;
import java.util.List;


public interface IPersonaDao {
    
    public List<PersonaDTO> select();
    
    public int insert(PersonaDTO persona);
    
    public int update(PersonaDTO persona);
    
    public int delete(PersonaDTO persona);
    
    
    
    
    
}
