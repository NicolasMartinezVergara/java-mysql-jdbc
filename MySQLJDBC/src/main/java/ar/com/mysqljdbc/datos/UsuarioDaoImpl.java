package ar.com.mysqljdbc.datos;

import ar.com.mysqljdbc.domain.UsuarioDTO;
import java.sql.*;
import java.util.*;



public class UsuarioDaoImpl implements IUsuarioDao {
    
    private static final String SQL_SELECT = "SELECT id_usuario, username, password FROM usuario";
    private static final String SQL_INSERT = "INSERT INTO usuario(username, password) VALUES(?, ?)";
    private static final String SQL_UPDATE = "UPDATE usuario SET username = ?, password = ? WHERE id_usuario = ?";
    private static final String SQL_DELETE = "DELETE FROM usuario WHERE id_usuario = ?";
    
    private Connection conexiontransaccional;
    
    public UsuarioDaoImpl(){
    
    }
    
    public UsuarioDaoImpl(Connection conexiontransaccional){
        this.conexiontransaccional = conexiontransaccional;
    }
    
    @Override
    public List<UsuarioDTO> select() {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        UsuarioDTO usuario = null;
        List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        
        try {
            conn = this.conexiontransaccional != null ? this.conexiontransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                int id_usuario = rs.getInt("id_usuario");
                String username = rs.getString("username");
                String password = rs.getString("password");
                
                usuario = new UsuarioDTO(id_usuario, username, password);
                usuarios.add(usuario);
                
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }finally{
            Conexion.close(rs);
            Conexion.close(stmt);
            if(this.conexiontransaccional == null){
                Conexion.close(conn);
            }
        }
        return usuarios;
        
    }
    
    @Override
    public int insert(UsuarioDTO usuario) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
        
        try {
            conn = this.conexiontransaccional != null ? this.conexiontransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setString(1, usuario.getUsername());
            stmt.setString(2, usuario.getPassword());
            rows = stmt.executeUpdate();
            System.out.println("Cantidad de registros insertados: " + rows);
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }finally{
            Conexion.close(stmt);
            if(this.conexiontransaccional == null){
                Conexion.close(conn);
            }
            
        }
        return rows;
    }
    
    @Override
    public int update(UsuarioDTO usuario) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
        
        try {
            conn = this.conexiontransaccional != null ? this.conexiontransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_UPDATE);
            stmt.setString(1, usuario.getUsername());
            stmt.setString(2, usuario.getPassword());
            stmt.setInt(3, usuario.getId_usuario());
            
            rows = stmt.executeUpdate();
            System.out.println("Numero de registros actualizados: " + rows);
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }finally{
            Conexion.close(stmt);
            if(this.conexiontransaccional == null){
                Conexion.close(conn);
            }
        }
        return rows;
        
    }
    
    @Override
    public int delete(UsuarioDTO usuario) {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
        
        try {
            conn = this.conexiontransaccional != null ? this.conexiontransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_DELETE);
            stmt.setInt(1, usuario.getId_usuario());
            rows = stmt.executeUpdate();
            System.out.println("Numero de registros eliminados: " + rows);
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally{
            Conexion.close(stmt);
            if(this.conexiontransaccional == null){
                Conexion.close(conn);
            }
        }
        
        return rows;
    }
    
}
