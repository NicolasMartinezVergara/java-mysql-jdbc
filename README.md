Proyecto del curso “Universidad Java 2021, de cero a experto” (UDEMY)
Proyecto centrado en los fundamentos de una base de datos.

El objetivo de este ejercicio fue realizar una aplicación java para gestionar una lista de personas y de usuarios.
La idea principal fue trabajar más el concepto de base de datos con JDBC, recuperando los objetos desde la base de datos columna por 
columna (en lugar de objetos completos).
Fue el primer ejercicio de manejo de base de datos.

Desarrollado con Apache Netbeans.
Administrador de librerías Maven.
Base de datos utilizada MySQL 8 con MySQL Workbench.
Capa de datos utilizando JDBC.	

Las pruebas se realizan por medio de la clase ManejoPersonasTest en la ruta java-mysql-jdbc/MySQLJDBC/src/main/java/ar/com/mysqljdbc/,
todas pruebas por la consola standard.

Las clases de dominio o modelo están definidas sin anotaciones especiales, dado que no se está implementando ningún Framework específico.
Sólo sus atributos, métodos get y set, el método toString, un constructor vacío (y más constructores si fuesen necesarios).

Se realiza la conexión por medio de JDBC y Mysql Driver en la ruta src/main/java/ar/com/mysqljdbc/datos, clase Conexion (previa configuración 
en el pom.xml para MySQL).
Aquí se crea el método DataSource para el manejo de pool de conexiones, y se crean los métodos para abrir y cerrar los objetos de
tipo Connection, Statement y ResultSet (esto cada vez que se realiza una operación con el servidor).
MySQL en localhost:3306 .

En la capa de datos también se encuentran todas las Interfaces y clases DAO que implementan dichas interfaces.
En las clases se encuentran los métodos para recuperar datos, modificarlos, insertarlos y eliminarlos.
Por cada método, se abre y se cierra una conexión (todo declarado en el método).
Cada método es mandar a llamar a una constante de la clase, que almacenan un String de tipo SQL para que se ejecute esa sentencia
(SELECT, INSERT, UPDATE, DELETE, con sus respectivos parámetros), por medio de los objetos Connection, PreparedStatement y 
procesando el resultado con ResultSet.












